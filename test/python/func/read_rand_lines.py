#!/usr/bin/python

import sys
from fileutil import linereader

if len(sys.argv) != 3:
    print "usage: read_rand_lines.py <data file> <# of lines to read>"
    sys.exit(1)
data_file = sys.argv[1]
times = int(sys.argv[2])
print "reading %d lines from %s" %(times, data_file)
queue_name="test-queue"
linereader.startRandomReader(data_file, queue_name)
for i in range (0,times):
    print linereader.getRandomLine(queue_name)
