#!/usr/bin/python

# Copyright (C) 2009-2012 Travis Bear
#
# This file is part of the FileUtils prolect
#
# FileUtils is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# grinderScripts is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with grinderScripts; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


import unittest
import fileutil.reverse as reverse

class TestTail

    def setUp(self):
        #self.data_file = "../data/8_lines_w_blanks.txt"
        self.data_file = "../data/200_lines.txt"
        self.lines = open(self.data_file).readlines()
        self.queue_size = 2
        self.repeat_reads = False
        self.start_at_last_location = False
        
        
    def test_create_queue(self):
        queue_name = "single_queue"
        file_reader.startSequentialReader(self.data_file,
                                          queue_name,
                                          self.queue_size,
                                          self.repeat_reads,
                                          self.start_at_last_location)
        self.assert_(file_reader.seqential_line_queues.has_key(queue_name))
        line = file_reader.getSequentialLine(queue_name)
        self.assertEqual(line, self.lines[0].strip())
        file_reader.stopSequentialReader(queue_name)
        
        
    def test_two_queues_on_one_file(self):
        queue_one = "q1"
        file_reader.startSequentialReader(self.data_file,
                                          queue_one,
                                          self.queue_size,
                                          self.repeat_reads,
                                          self.start_at_last_location)
        self.assert_(file_reader.seqential_line_queues.has_key(queue_one))
        queue_two = "q2"
        file_reader.startSequentialReader(self.data_file,
                                          queue_two,
                                          self.queue_size,
                                          self.repeat_reads,
                                          self.start_at_last_location)
        self.assert_(file_reader.seqential_line_queues.has_key(queue_two))
        line_one = file_reader.getSequentialLine(queue_one)
        line_two = file_reader.getSequentialLine(queue_two)
        self.assertEqual(line_one, self.lines[0].strip())
        self.assertEqual(line_two, self.lines[0].strip())
        self.assertEqual(file_reader.getSequentialLine(queue_one), self.lines[1].strip())
        file_reader.stopSequentialReader(queue_one)
        file_reader.stopSequentialReader(queue_two)
    
    def test_read_queue_repeat(self):
        queue_name="repeater"
        file_reader.startSequentialReader(self.data_file,
                                          queue_name,
                                          self.queue_size,
                                          True,
                                          self.start_at_last_location)
        for i in range(1,20):
            line =  file_reader.getSequentialLine(queue_name)
            num = int(line)
            self.assertTrue(num > -1)
        file_reader.stopSequentialReader(queue_name)


class TestRandomReads(unittest.TestCase):

    def setUp(self):
        data_file = "../data/200_lines.txt"
        self.queue_name="random"
        file_reader.startRandomReader(data_file, self.queue_name)

    def test_read(self):
        for i in range (1,4000):
            line = file_reader.getRandomLine(self.queue_name)
            self.assertNotEqual(line, '')
            num = int(line)
            self.assertTrue(num > -1)
        file_reader.stopRandomReader(self.queue_name)
            


if __name__ == '__main__':
    unittest.main()


    
